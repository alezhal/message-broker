﻿using MassTransit;
using System;
using System.Threading.Tasks;
using Pcf.ReceivingFromPartner;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Pcf.Administration.WebHost
{
    public class PartnersConsumer : IConsumer<ReceivingFromPartnerDto>
    {
        private readonly IRepository<Employee> _employeeRepository;

        public PartnersConsumer(IRepository<Employee> employeeRepository) => 
            _employeeRepository = employeeRepository;

        /// <summary>
        /// This consumer get message from bus (MT RabbitMQ) instead of synchronous http request 
        /// 'AdministrationGateway.cs: NotifyAdminAboutPartnerManagerPromoCode()' ->
        /// -> over synchronous http
        /// -> 'EmployeesController.cs: UpdateAppliedPromocodesAsync(Guid id)' 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Consume(ConsumeContext<ReceivingFromPartnerDto> context)
        {
            var employee = await _employeeRepository.GetByIdAsync(context.Message.PartnerManagerId);
            employee.AppliedPromocodesCount++;
            await _employeeRepository.UpdateAsync(employee);
        }
    }
}
