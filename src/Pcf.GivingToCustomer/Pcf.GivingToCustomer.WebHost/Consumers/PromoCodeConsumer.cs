﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Pcf.GivePromoCode;
using System.Linq;
using System.Threading.Tasks;

namespace Pcf.GivingToCustomer.WebHost.Consumers
{
    public class PromoCodeConsumer : IConsumer<GivePromoCodeDto>
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromoCodeConsumer(
            IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository, 
            IRepository<Customer> customersRepository) => 
            (_promoCodesRepository, _preferencesRepository, _customersRepository) = 
            (promoCodesRepository,preferencesRepository, customersRepository);

        /// <summary>
        /// This consumer get message from bus (MT RabbitMQ) instead of synchronous http request 
        /// 'GivingPromoCodeToCustomerGateway.cs: GivePromoCodeToCustomer(PromoCode promoCode)' ->
        /// -> over synchronous http
        /// -> 'PromocodesController.cs: GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)' 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Consume(ConsumeContext<GivePromoCodeDto> context)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(context.Message.PreferenceId);
            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));
            PromoCode promoCode = PromoCodeMapper.MapFromConsumer(context.Message, preference, customers);
            await _promoCodesRepository.AddAsync(promoCode);
        }
    }
}
