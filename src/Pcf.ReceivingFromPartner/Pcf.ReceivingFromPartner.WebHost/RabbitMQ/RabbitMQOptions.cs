﻿namespace Pcf.ReceivingFromPartner.WebHost
{
    public class RabbitMQOptions
    {
        public string Host { get; set; }

        public string AppliedPromocodesCountQueueUri { get; set; }

        public string PromoCodeQueueUri { get; set; }
    }
}
